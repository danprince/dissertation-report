For the next chapter of this report, the design for the platform will be discussed at detail, as well as the reasonings behind the decisions and some of the conclusions drawn from the background research.

\section{Requirements}
\begin{itemize}
	\item There must be the ability to have multiple sessions, so that one server can house a number of contexts.
	\item Clients must be able to connect over a mobile phone (Android).
	\item A secure interface should be provided for administrators	
	\item Sessions should be controlled by administrators
	\item Audio delivery should be controlled by administrators
	\item Administrators should have access to an interface that allows them to compose tracks from the other pieces of audio.
\end{itemize}

\section{Localisation}
\subsection{Description}
The devices will be positioned using a form of reactive SONAR developed for this project. Once the devices have connected to the network, they will sequentially emit a ping. The other devices will record the time at which they heard this ping and use that to calculate their distance from the other devices within the space. Once the relative distances have been calculated for all of the devices, it is possible to intersect the circles to trilaterate the positions of each device. Due to the lack of information, it is not possible to position the devices in their actual orientation, this factor must instead be accounted for by the user controlling the platform.

\subsection{Distances}
To calculate the distances between two devices within a space, a sonic pulse is emitted from the first device, whilst the second starts recording. Once the pulse is detected within the recording, the recording is halted and the audio is processed to determine the time at which the pulse was heard. 

\ref{timeline} is a timeline diagram, showing an example of the times at which the differing events could occur.

\begin{figure}[ht!]
\centering
\includegraphics[width=\linewidth]{images/timeline.png}
\caption{Event Timeline}
\label{timeline}
\end{figure}

\begin{figure}[H]
\begin{center}
    \begin{tabular}{| l | l | l |}
    \hline
    Time & Device & Event \\ \hline
    0   & 1 & A - Starts Listening \\ \hline
    0   & 2 & B - Emits Ping \\ \hline
    300 & 1 & C - Hears Ping \\ \hline    
    0   & 2 & D - Starts Listening \\ \hline
    400 & 1 & E - Emits Ping \\ \hline
    800 & 2 & F - Hears Ping \\ \hline
    \end{tabular}
\caption{Time Data}
\end{center}
\end{figure}

For the purposes of this example, we assume that the time it takes to correspond with the server to relay information, is negligible. In actuality, this will be accounted for.

Device 1 now knows that it took 300ns for the ping to arrive and that it took 400ns seconds to hear the ping from Device 2. To eliminate inconsistencies here, it is safest to take the average and assume that the ping took 300ns to travel from one to the other. The distance can be calculated by multiplying the speed by the time it took. Based on the density of air at room temperature, for this example the speed of sound is taken to be 340.29 m/s.
	
\begin{equation}
\begin{split}
distance& = speed \times time\\
 & = 340.29 \times 0.0035\\
 & = 1.191015
 \end{split}
\end{equation}

It is possible to say with reasonable certainty, that the devices are 1.191015 metres away from eachother. This value can then be used in the next step, to localise the devices.

\subsection{Network}
The same principle can be applied to each device within the network to create a full table of distances. This algorithm will have a complexity of $O(n^{2})$ as every device has to interact with all of the other devices in the network. It would be possible to reduce the complexity to $O((n^{2}) - n)$ by doing only one test for each device pairing, this however, would reduce the accuracy of the positioning.

\subsection{Intersecting the Circles}

Consider the device configuration in Figure 3.2

\begin{figure}[H]
\centering
\includegraphics[width=90mm]{images/local-1.png}
\caption{Device Configuration}
\label{local1}
\end{figure}

Once a full set of distances has been recorded, the virtual device map can begin to be created. Taking the first device to be at the origin (0,0) it’s possible to use the distance to the second device as the radius, from which, a circle can be constructed.

\begin{figure}[H]
\centering
\includegraphics[width=90mm]{images/local-2.png}
\caption{First distance}
\label{local2}
\end{figure}

In the diagram above, a single device is shown as A. The distance from A to device B is denoted as the radius of the circle and the dashed line is the line on which B must lie. Initially there is not enough information to position device B on that circle. So B may be positioned at any arbitrary point on that circle.

\begin{figure}[H]
\centering
\includegraphics[width=90mm]{images/local-3.png}
\caption{Devices being positioned}
\label{local3}
\end{figure}

Next, distance circles are constructed around the two devices that have been positioned between themselves and point C. The arrows denote the distances from the devices to the device in question. 

There are two clear points of intersection marked with x and y. Device C must reside at one of these locations, again, there is not enough information presented to be sure of which intersection it is, so an arbitrary choice is made and the device is positioned at either of the intersections.

\begin{figure}[H]
\centering
\includegraphics[width=90mm]{images/local-4.png}
\caption{Trilaterated}
\label{local4}
\end{figure}

Now that C is positioned, the process becomes simplified as there are now 3 positioned devices and the resulting distance circles can be used to trilaterate the position of the other devices present. The above diagram shows the location of device D using the intersection the 3 located devices, as marked by the green circle.

\section{Audio Format}
The primarily supported audio format for this project will be Vorbis, based on the fact that it performs as well (if not better) than any of the other avaiable high quality codecs.

This also means that when Opus becomes supported, files of that format will work seamlessly.

However, whilst Ogg contained formats will be the format of preference, MP3 will \textbf{also} be supported, justified only, by the fact that the sheer amount of MP3 material can only contribute to the experience of the platform, despite the fact that it goes against the ethic of an open source project like this.

\ref{audio-support-table} is an abridged support table for the two formats.
\begin{figure}
\begin{center}
    \begin{tabular}{| l | l | l |}
    \hline
    Browser & MP3 & Ogg \\ \hline
    Internet Explorer &  & No \\ \hline
    Chrome & Yes & Yes  \\ \hline
    Firefox & Partial & Yes  \\ \hline
    Safari & Yes & No  \\ \hline
    Opera & No & Yes  \\ \hline
    \end{tabular}
    \label{audio-support-table}
    \caption{Browser Audio Support}
    \small \url{http://caniuse.com/ogv}
\end{center}
\end{figure}

As the targeted Phone OS will be Android, the most important browser to review is Chrome, which supports both formats. This does however mean that iPhone users will have to use the Chrome application, rather than the native Safari app, as Safari does not support Ogg containers.

The nature of the implementation for the audio should allow the format to be easily changed for a more commercially viable format, with better support on mobile devices.

\section{Delivery Maps}
Within the scope of this project, the concept of a delivery map is defined as a visual representation of the room and devices, with a binary mechanism for differentiating between target zones and ignored zones.

\begin{figure}[ht!]
\centering
\includegraphics[width=90mm]{images/dmap-1.png}
\caption{Delivery Map}
\label{dmap1}
\end{figure}

\ref{dmap1} shows the design for a delivery map with a number of regions marked as target zones (areas of blue). This approach is not limited to using radial zones and could be extended to support complex polygonal selections, although this would increase the complexity of the data that would have to be stored as well as the devices-in-zone calculations.

\begin{figure}[ht!]
\centering
\includegraphics[width=90mm]{images/dmap-2.png}
\caption{Delivery Map}
\label{dmap2}
\end{figure}

Once the devices have been localised, they effectively start to exist within 2d virtual space. The delivery map can be mapped to the bounding rectangle of the devices, as shown in \ref{dmap2}. Once the mapping transformation is complete, it is a matter of simple trigonometry to calculate whether the devices are within the shaded regions.

\begin{figure}[ht!]
\centering
\includegraphics[width=90mm]{images/dmap-3.png}
\caption{Delivery Map}
\label{dmap3}
\end{figure}

\ref{dmap3} shows a portion of the process of calculating whether the devices fall within the shaded areas. The radii of the respective circles are marked with red. For each circle, the euclidean distance between the center of the circle and each of the devices is measured. If the distance is found to be less than the radius, then the device is within the shaded region. \ref{dmap3} shows that devices D, E, F and G are within the shaded region (for the clarity of the diagram, not all measured distances were shown).

\section{Audio Delivery}
Talk about requesting a stream from the admin

\section{Real Time Communication}
The platform communication protocols can be found in Appendix \ref{protocols}.

\section{Application}
\subsection{Storage}
This project requires the use of two kinds of storage, a persistent database server and associated session information for connected clients. This will be handled with the following technologies.

\subsubsection{MongoDB}
Mongo will be used to store all the necessary documents pertaining to the application, despite the fact that Mongo is a schemaless database, here are some loose schemas (subject to change) that will represent the types of data that will be stored within Mongo.

\subsubsection{Administrators}
Administrators will be the users who have access to the admin application. Full control is given to the admins, meaning they can edit any of the data in the database. Authentication must therefore be implemented and the database will have to store the credentials alongside the Mongo generated id.

\lstset{language=Javascript}
\begin{lstlisting}
"admin": {
    "_id": [STRING],
    "name": [STRING],
    "password": [STRING]
}
\end{lstlisting}

\subsubsection{Audio}
Audio files are the building blocks for song structures. Administrators can upload audio files for themselves and other administrators to use within song compositions. Once uploaded, the actual audio file will be stored on the server and the path must be saved within the database. A name will also be saved, alongside a duration, so that meaningful displaying and sorting of audio data is possible. 

\begin{lstlisting}
"audio": {
    "_id": [STRING],
    "name": [STRING],
    "path": [STRING],
    "duration": [NUMBER]
}
\end{lstlisting}

\subsubsection{Songs}
Finally, songs are the staple of the application. The song format will be saved with a name and a duration, then also a timeline attribute. The timeline specifies a non-linear collection of start times and delivery maps for that file. Delivery maps in themselves are complex structures, so they have been left out of this structure, but will be explained exclusively in another section.

\begin{lstlisting}
"song": {
    "_id": [STRING],
    "name": [STRING],
    "duration": [NUMBER],
    "timeline": {
    	   "<id>": {
            "start": [NUMBER],
            "dmo": "<id>"       
        }
    }
}
\end{lstlisting}

\subsubsection{Delivery Maps}
The delivery maps are stored as a series of identifiers that define geometric primitives and their positions.

\begin{lstlisting}
"delivery_map": {
    "_id": [STRING],
    "position": {
    	   "x": [NUMBER],
        "y": [NUMBER]
    },
    "radius":[NUMBER]
}
\end{lstlisting}

The system may be extended to support polygons.

\begin{lstlisting}
"delivery_map": {
    "_id": [STRING],
    "points": [ [[NUMBER], [NUMBER]], ... ]
}
\end{lstlisting}


\subsection{Localstorage/Cookies}
Non-sensitive data specific to clients will be stored locally, Localstorage should be offered with a fallback to cookie storage if necessary, as Localstorage is a HTML5 feature.

\subsection{Authentication}
The administrator interface will be wrapped in an intemediary level of session based HTTP authentication, which will be required before accessing any of the services which require WebSocket connections. Once authenticated, there will have to be a second level of verification as the socket connections are made, to verify that the authentication session variable exists.

\subsection{HTML5}
\subsubsection{WebSockets}
WebSockets will be used to do the majority of the communication between the admin application and the clients, with the server acting as a mediator for all outbound and inbound messages.

\subsubsection{Canvas}
The canvas element will be used to allow administrators to draw delivery maps for their composed tracks within the admin interface.

\subsubsection{Audio}
The HTML5 audio element will be used for playing audio.

\subsection{User Management}
Admins
Controllers
Listeners

\subsection{Structure}
Hub
Admin Panel
Compose

\section{Implementation Languages}
\subsection{Client}
The client will be a web application, written in Javascript and leveraging the capabilities of the HTML5 specification. It should be designed to run on mobile devices, so that if necessary it can be packaged as a PhoneGap application, in order to also provide a Native Application version.

\subsection{Server}
The server will also be written in Javascript, running ontop of Node.

\subsection{Web Application}
From this point onwards 

\subsection{Database}
MongoDB will be used as a database, which in turn will double up as a session variable store for the admin interface.

\section{Libraries}
\subsection{Server}

\subsubsection{Express}
Express\cite{express} is a lightweight Node web framework, built on top of connect\cite{connect} with flexibility in mind. It provides a robust set of features for building web applications with. As an open source project with hundreds of contributors, and as the best supported web framework for Nodejs, Express was a clear choice.

\subsubsection{Socket.io}
Socket.io\cite{socketio} is a WebSocket library that provides fallbacks (Long polling XHR and Flash) for older browsers, alongside a very useful set of features such as custom event names, namespacing and socket broadcasting. Together this makes SocketIO a powerful library for handling real time communication.

\subsubsection{EJS}
EJS\cite{ejs} is a templating language which allows the writing of regular HTML with the option to insert variables using EJS’ templating syntax. It will be used to create all of the views and partials.

\subsubsection{MongoJS}
MongoJS\cite{mongojs} js a wrapper around the official Node Mongo driver. It makes talking to a Mongo database from Node easier.

\subsubsection{Session.socket.io}
Session.socket.io\cite{session-socket-io} wraps a layer around the regular socket.io api, allowing session based authentication to be done. This will be important in securing the administrator interface.

\subsubsection{bcrypt}
bcrypt\cite{bcrypt-node} is the de facto encryption library across most programming languages (citation needed)\cite{bcrypt}. It will be used to generate hashes and salts for storing passwords.

\subsubsection{node-ffprobe}
Node-ffprobe\cite{node-ffprobe} is a Node wrapper of ffprobe and it provides an api for extracting data from MPEG format files. It’ll be used to obtain the metadata from MP3 files.

\subsubsection{Q}
The async nature of Node means that it is easy to end up lost in a callback saturated block, making it difficult to write code and harder to debug.

A callback generally takes the form


\begin{lstlisting}
fs.readFile(fileName, function(err, contents) {
    ...
});
\end{lstlisting}

Where the second argument supplied to the readFile function is the callback. This is fine if only one level of depth is necessary, but let’s suppose the program needs to write to a file once the contents have been read?

\begin{lstlisting}
var file;
fs.readFile(fileName, function(err, contents) {
    file = contents;
});
fs.writeFile(otherFile, file, function(err) {
    console.log('done');
});
\end{lstlisting}

This may seem like the logical way to handle this, but in fact, it won’t work. The writeFile function will be called before the readFile function has finished and the value undefined will be written into the file. Instead, the next call must be situated within the callback for the first function.

\begin{lstlisting}
fs.readFile(fileName, function(err, contents) {
    fs.writeFile(otherFile, contents, function(err) {
        console.log('done');
    });
});
\end{lstlisting}

It’s now clear that the write operation will happen when (and only when) the callback function from readFile is called. As you can imagine, this becomes arduous when you are 3+ levels deep inside callbacks. Code must be very well thought out to overcome this.

Promises circumvent some of the nature of Javascript callbacks.

\begin{lstlisting}
fs.readFile(fileName)
.then(function(contents) {
   fs.writeFile(otherFile)
   .then(function() {
       console.log('done');
   });
});
\end{lstlisting}

While it may look equally messy to an untrained eye; Promises do make writing async code a lot easier. Q\cite{Q} is the standard library to use when in need of Promises.

\subsection{Client}
\subsubsection{Angular}
AngularJS\cite{angular} is a Google backed, self-styled \textit{“Super Heroic MVW Framework”} that has taken front end development by storm over the last 6 months (citation needed). It allows clean separation between modular components, with a sprinkle of powerful two way binding between the DOM and your application layer.

Most of Angular’s success comes from it’s ability to clearly distinguish between and create reusable components which fall into the following 5 classes.

\textbf{Directives}\\
A directive defines a custom element or attribute and allows you to shape its behaviour when used in a document. Directives enable developers to build complex yet maintainable user interfaces from small components with clearly defined logic.

\textbf{Controllers}\\
Controllers can be fixed to areas of the DOM, they then proceed to expose a \$scope object which allows developers to perform a number of different levels of binding between variables in memory and elements within the DOM. For example, two way binding between an input form and a search string variable: whenever the value inside the form is changed, so is the state of the search string variable in memory. Vice versa, when the value of the variable is changed in memory, the input will update to reflect this.
 
\textbf{Filters}\\
Filters provide a way to transform data(sets) within the view (not in memory). For instance the price of a product may be stored within a database as an integer representing GBP. To make the application more accessible to users who operate in other currencies a currency filter could be created, which would convert all prices into the currency of choice, based on an exchange rate argument.

\textbf{Services}\\
Services are the most complex part of Angular and can be subdivided into three main types, Services, Factories and Providers. These still however, tend to fall under the umbrella term of services. Services are the model layer of your application and can be used as adapters for both external and internal data sources, or for providing a logic based service, abstracted away from your DOM bound controllers. Services are designed to be reusable and accessible from anywhere within your application.  

\textbf{Modules}\\
Modules are the container into which all of the above components fit. A module can have as many directives, filters and services as necessary. A module can then be injected into an application, which will expose it’s child components to the application in question.

This loosely coupled structure, combined with some intelligent features, such as argument based dependency injection and access to the controlling mechanisms (\$scope, \$rootScope, \$digest, *.\$apply, \$watch) mean that Angular is an undeniably powerful way to create web applications.

\subsubsection{Socket.io \& Q}
The benefits and applications of both of these libraries have already been discussed in the server side section. In Socket.io’s case this is the client library which complements the server side library in use. 

For the same reasons as the server side, Promises will be used within the client side code, using the Angular, \$q service. Many of Angular’s default services come with a promise wrapper, meaning that to keep code consistent, it is therefore important that the libraries written for this project also use Promises.