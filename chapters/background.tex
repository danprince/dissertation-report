\section{Background}
During the background section of this report, the primary focal points necessary to solving this problem with a useable solution, will be further explored. Starting with the format of the audio files and continuing on to device playback, streaming and syncing technologies, storage of data and delivery mapping techniques. Finally, an in depth study of the potentially suitable programming languages will be made in an attempt to determine the appropriate technologies with which to build this platform.

\subsection{Definitions}
A codec is the instance of a pairing of an encoder and decoder for a particular format of audio.

\subsection{Audio Formats}
The fundamental basis for this project is the distribution of audio, which means that an appropriate audio format must be used consistently across the platform, or alternatively the platform must be able to adapt to suit multiple formats. Here are some considerations on the more popular formats along with some observations on the quality of their compression and the clarity of their sound.

\subsubsection{MP3}
MP3\cite{mp3} is perhaps the world’s best known audio format, due to the traction it gained as a consumer format, popularised largely by the online music retailers who offered their downloads in this format, partnered with the corporations who retailed devices with which to play them. As a result, the support for the MP3 format is consistent across many platforms and MP3 codecs can be found almost everywhere.

MP3 (MPEG-2 Audio Layer III) uses a lossy data compression algorithm to reduce the size of the original recording. The algorithm relies on psychoacoustic models to filter out sections of the frequencies, which the models dictate to be beyond the auditory resolution of the average person. In theory, this means that the audio file won’t have changed audibly, however data will still have been lost.

A 128kb/s MP3 sampling of a CD recording from an original source yields an 11:1 compression ratio. This has clear benefits where transmission and storage are concerned.

MP3, however, is not an open standard and has therefore been the source of contention between a number of corporations\cite{mp3-patent} where licensing and patents are involved, with many companies claiming to own the rights and actively enforcing the patents.

Other similar formats considered too similar to compare (mp3PRO, AAC, MP2)


\subsubsection{WAV}
WAV\cite{wav} is another commonly seen format, mostly associated with Microsoft platforms. As the primary format for storing raw audio on the Windows operating system, support for WAV files is also fairly consistent.

WAV (Waveform Audio File) is often employed for high quality archival of the data, in situations where size is not a factor. It is ideal for situations where time spent compressing and uncompressing the data is a factor (such as audio editors). 

The uncompressed nature of WAV files means that the clarity of the audio will always be greater (than a lossy compression format), but comes with the cost of increased requirements for storage and transmission. This renders the WAV as an uncommon format for streaming, however the same reasoning also means that it is an ideal format for audio analysis.

Other similar formats considered too similar to compare (AIFF, RIFF)


\subsubsection{Vorbis}
Vorbis is one of the lesser used formats within consumer driven music distribution and was born out of fears involving the impending licensing fee restrictions on the MP3 format. 

Vorbis (like MP3) uses lossy compression to eliminate unnecessary frequencies, but has been proved to perform to significantly better effect than most other lossy audio formats.

Notable audio streaming companies such as Spotify use the Vorbis format to deliver content to millions of users every day \cite{spotify}.

In the near future, Vorbis may be superseded by the Opus format. Created by the same team as Vorbis, Opus is designed to be totally compatible with applications that currently use the Vorbis codec. Beyond Vorbis, Opus will offer a larger range of compression and sampling options, with a wider band of sampling rates that most other conventional audio formats\cite{opus-faq}.

\begin{figure}[ht!]
\centering
\includegraphics[width=90mm]{images/opus-comparison.png}
\caption{Comparison of Codecs}
\small \url{http://www.opus-codec.org/comparison/}
\small (Licensed under Creative Commons)
\label{codec-comparison}
\end{figure}

As shown in \ref{codec-comparison} that in terms of performance a high-bitrate, Opus, Vorbis, MP3 and AAC are the clearly superior formats. Opus however, supports near linear trend of qualities to appropriate bit rates, meaning that it would be possible to provide audio encoded at a very low bitrate, specifically with potentially limiting 2g or 3g networks in mind.

Currently, only Firefox supports Opus format data \cite{opus-support}, but support is planned for the next build of Chrome \cite{opus-support} and Chrome Canary now ships with support. As both Vorbis and Opus formats are contained within Ogg containers, they will function as drop in replacements for applications that currently use Vorbis.

\subsection{FLAC}
FLAC (Free Lossless Audio Codec) is a lossless alternative to the lossy compression methods here (MP3, Ogg Vorbis), meaning that it can be decompressed to an identical copy of the original file. However it still yields a 3:5 compression rate. 

FLAC is ideal for archival as it allows original sources to be compressed for storage, but with the option to decompress to retain the original source once again in the future. The increased file size has meant that FLAC has not been widely used as a transmission format. However, many recording artists will record and distribute their live performances as FLAC, and it remains the optimal format for distribution of lossless audio. 

\subsection{MIDI}
MIDI (Musical Instrument Digital Interface) represents a unique solution space within this problem domain, as the format is relatively lightweight making it a contrasting choice in comparison to the others here. Rather than sampling raw audio and converting it into numerical digital representations, MIDI defines the interface, and the audio is written for it.

This would make a large difference within this project, as audio data would not need to be streamed in the conventional way, as it would no longer be raw/encoded samples, but rather a set of event signals representing pitch and velocity.

The large downside to MIDI is that most smartphones and computers don’t have a MIDI synthesizer by default, therefore this project would require writing some, or finding a freely licensed collection to use. This would be difficult and also limits the potential of the audio experience to the set of synths used in the project.


\section{Playback}
Once the audio has been transferred to the a client, the format is decoded (providing there is a codec available) and and the raw audio is played. A selection of codecs will usually be present in both the native application libraries and also the browser installed on devices.

Mobile devices can usually play audio from a stream, whether that is a read stream from a file, or a download stream from a network.

\section{Streams}
A stream is defined as interface with a source and a sink. The data is sent from the source to the sink, where it is received and processed, or discarded.

A stream enables the linear processing of data while it is still being transferred.

\begin{figure}[ht!]
\centering
\includegraphics[width=90mm]{images/stream.png}
\caption{Stream Representation}
\label{stream-figure}
\end{figure}

\ref{stream-figure} is an abstract example of how audio may be played and streamed.

The audio is read from a file, or encoded from media input at the source, and is transferred byte by byte to another endpoint (local or foreign), at which the audio is decoded. Then, with the raw audio present, it can be played, before the data is discarded from memory as the stream reaches the sink.

\ref{stream-server} is an example of what this process looks like in asynchronous code (Node JS).

\lstset{language=LiveScript}
\begin{lstlisting}[caption={Server},label={stream-server}]
stream = (res, req) ->
 
 path = './audio/test.mp3'
 stat = fs.statSync path
    
 console.log "Streaming " + path
 
 res.writeHead 200, {
  'Content-Type': 'audio/mpeg'
  'Content-Length': stat.size
 }
 
 readStream = fs.createReadStream filePath
 readStream.pipe res
\end{lstlisting}

\begin{lstlisting}[caption={Client},label={stream-client}]
audio = new Audio '/url'
audio.on 'ready' ->
 audio.play!
\end{lstlisting}

A request is made and passed to the function along with a response object that can be used to write data back to the client. A statistics object is created for the file so that the application can respond with a header, telling the client how long the audio file about to be streamed is.

Finally, the ‘source’ is created as a read stream. This stream object will emit events when certain occurrences happen. It is ‘piped’ to the response object. What this means is that every time the read stream emits a “data” event, the corresponding bytes of data will be written to the response.

The client makes an audio object which points to some url (wherever the server is), waits for a there to be data available in the stream, then starts to play it.

This encapsulates the last section of \ref{stream-figure}, the codec, the raw audio and the sink are all located here.

\section{Synchronisation}

When distributing audio to a room of devices at close proximity, it is important that the playback is perfectly synchronised. Henceforth, a definition for perfect synchronisation must be decided upon. There are a plethora of different contexts it could exist within, however, within the scope of this project, perfect sync will constitute as an undetectable latency, as judged by the human ear.

Despite the fact that the human ear can differentiate between a 0.1ms disparity in synchronisation; it is possible to have up to 25-50ms of latency before the brain will process the sound as separate sources \cite{ears-sync}. To discover what the human brain would find acceptable as a maximum latency, a test was devised to calculate just how noticeable it became.

The test involved playing two pieces of audio with a variable time delay between them. Simulated latencies of 0 to 120ms were measured. The four test subjects were asked to describe, as a percentage, how in sync the audio was (where 100\% is perfectly). \ref{latency-test-figure} shows the results.

\begin{figure}[ht!]
\centering
\includegraphics[width=\linewidth]{images/latency-test.png}
\label{latency-test-figure}
\end{figure}

If 60\% is taken as the minimal acceptable level of apparent sync, then it is clear to conclude that the latency must never be higher than $60ms$. For a seamless experience, however, the latency needs to be tending to $<40ms$, where the average (purple line) approaches 100\%.

\section{Development Technologies}
The core requirement of this project is that it must run on mobile devices, the more devices that are supported, the better. There will also be a server side component which handles the data storage, synchronisation signals and session management. The server will have an administrator interface. Ideally, each component of this project will be kept modular, allowing it be reused and extended in the future.

\subsection{Client}
\subsubsection{Java}
Java is natively supported on Android Devices and allows developers to write type-safe static code on a tried and tested virtual machine. Java’s performance is good, especially when the code is written to adhere to the optimisation idiosyncrasies of the JVM \cite{java-benchmark}. However, package management with Maven has been called "nothing short of a disaster"\cite{maven} and Java still fails to provide sensible support for streams, with high level language concepts such as Lambdas and a sane streaming interface only appearing for the first time in Java 8 \cite{java8-stream}. Java itself is not a viable option for this project. However, Java’s younger siblings Clojure and Scala could be considered as alternatives. Both languages have embraced the functional programming revolution \cite{fp-revolution} and also bring a clearer, more concise syntax to the table, which compiles down to JVM bytecode.

\subsubsection{Clojure}
Clojure is a LISP (LISt Processing programming language) which compiles to JVM bytecode, this means it is possible to develop Android applications with Clojure and a number of development environments exist, which make this a painless experience. Despite being an impure functional programming language, Clojure houses a very strong set of flexible data structures \cite{clojure-data} and concurrency methods \cite{clojure-concurrency}, making it easy for programmers to write both clean and safe code. It’s foundations as a LISP make it ideal for meta-programming and sculpting the language to suit the problem domain at hand. Clojure has support for Streams, but it is fairly immature and this could lead to problems further down the road.

\subsubsection{Scala}
It’s worth noting, that both Scala and Clojure have seamless Java interoperability \cite{clojure-interop} \cite{scala-interop}, meaning that if a module or library exists in Java, they can hook into it, to leverage it from their own code bases. This is more of a key feature in Scala than Clojure however, as Scala is a multi-paradigm language, employing both functional and object oriented approaches. Scala has had streams built into the standard library since day one. Environments such as Scaloid were built to encourage Scala development for Android devices and have so far, gained a large amount of traction \cite{scaloid-github}.

\subsubsection{C\#}
The next runtime to consider is the Windows phone. The standard for developing Windows applications is through Visual Studio, equipped with C\#. C\# is a modern language that has had the chance to learn from Java’s misgivings, addressing many of the issues developers had raised in the past, without creating a language that felt alien. A competent Java programmer would be able to maintain a C\# project with minimal supervision. Streaming is well supported in C\#. However, to write Windows phone applications, C\# must leverage the .Net framework, which is only supported on Windows. The Mono project attempts to bring support to Unix, but has so far been inconsistent, and this level of inconsistency means that C\# probably not a good choice for this project.

\subsubsection{Objective C}
Finally, the last of the trinity of mobile development languages. Objective C for iOS. A language that is riddled with clunky syntax \cite{objc-dot} and an inflexible type system \cite{objc-opinions}. The community has done more damage to the language than it has been useful, in terms of standardisation and convention, spurred mainly by the sheer number of developers that are attempting to develop apps for iOS. Writing Objective C within XCode requires both an iOS developers license and a OS X development machine, neither of which are available for this project, therefore Objective C must also be excluded as a potential choice.

The next few choices are slightly more left wing approaches, differing in concept from the above languages.

\subsubsection{Python (Kivy)}
Kivy is a cross platform (Linux, Windows, OS X, iOS and Android) application framework which has been designed so that developers can write their apps in Python. Python is a great language for flexibility and productivity. Kivy lends itself to rapid prototyping. Combined with a large standard library and arguably a very readable syntax. Python with Kivy could be a good choice for this project.

\subsubsection{Haxe}
Haxe is a language that attempts to be the hybrid of all of a number of popular languages. Wielding a syntax that is half Java and half Javascript, Haxe's transpiler targets  Javascript, Flash, C++, C\# and Java. This means Haxe hits all of the above platforms out of the box. This comes at the expense of the transpilation (or cross compiling), which means that you effectively lose control of the optimisations you could have made if you were developing in the language natively. Again, it’s a fairly immature language that could use some large exposure from big projects, but it still represents an interesting choice for this project.

\subsubsection{Javascript}
The last approach that will be considered, is the web based approach. Pretty much every mobile device that is capable of browsing the internet has a Javascript runtime of some description, whether it’s Google’s V8\cite{google-v8}, Mozilla’s (Spider/Jager)monkey\cite{spidermonkey}\cite{jagermonkey}, or Microsoft’s Chakra\cite{chakra}, or even perhaps one of the more archaic engines that ship with older phones. A web based solution is the most cross platform solution. As a language, Javascript has more than it’s fair share of ugliness, mostly down to poor decisions from the language designers\cite{js-misunderstood}. Nonetheless, at heart it is an elegant language which can be shaped to fit multiple programming disciplines. With a powerful Prototypal inheritance system to first class support for Scheme\cite{scheme} style object manipulation, it represents a very flexible language choice. Recent advances with platforms such as Phonegap\cite{phonegap} and Titanium\cite{phonegap} have made it possible to write web based applications and then build native applications for deployment to all popular makes of Smartphone. The nature of web based applications means you can test them locally before deployment, or even test remote devices from within your browser, through utilities such as WEINRE\cite{weinre}. The combination of these features make Javascript a strong choice for this project.

\subsection{Server}
This project requires concurrency as an absolute essential, whichever language is picked will need strong concurrency capabilities. The standard way to achieve this today, is through asynchronous Event-Driven programming \ref{event-driven}. This causes I/O not to block and therefore makes the code flow asynchronous. Take the two following simple (albeit contrived) code examples


\begin{lstlisting}[caption={Synchronous},label={sync-code}]
while true
 socket = server.accept!
 result = db.query '...'
 socket.write result.object
 socket.close!
\end{lstlisting}


\ref{sync-code} is written in a blocking, synchronous style. The code is going to block on every one of those lines, meaning that if a new client connects before the database query and write has happened, then they will not receive a response until the last set of blocking requests have finished, the first socket is closed and the loop has gone round again.

Alternatively, here is an asynchronous example.


\begin{lstlisting}[caption={Asynchronous},label={async-code}]
handle = (socket) ->
 db.query '...' .then (result) ->
   socket.write result.object
   socket.close!
 
socket.accept handle
\end{lstlisting}


A handler function is created which runs an async database query then writes the result out and closes the socket, the same as before. However, without code that blocks, the socket.accept function can call the handle callback as many times as it needs, without having to wait for any other code to have finished. Every CPU operation is added as an event, which will be processed in queue. This allows code to run in a non-linear fashion, making it possible to support hundreds and thousands of concurrent connections, without having to worry about large wait times. Of course, the synchronous code can be run on multiple threads, but then this means inter thread communication, which can prove to introduce more problems than it solves. Event-driven libraries tend to run on a single thread (citation needed). Primarily, only event driven libraries will be considered here.

\subsubsection{PHP (React)}
PHP has been a staple server side language for many years, meaning it has accrued a gigantic community for support and development. However, it still harbours terrible inconsistencies \cite{php-bad}, left over from the days before PHP5, when there was no object oriented support. Speed and project structure are also concerns, however the abundance of frameworks make this an easier job than it used to be. Sadly, PHP doesn’t have a de-facto packager either, making it more difficult to find centralised libraries.

React (not to be confused with the  Google MV* Javascript framework), makes a valiant effort to have PHP feel more like its younger, more dynamic counterparts. There are no structured frameworks for writing React however, which means rolling a custom one would be necessary. Despite being a sensible way to write PHP, the failures of the language in the areas mentioned above, mean it won’t be used in this project.

\subsubsection{Python (Tornado/GEvent)}
Again, a clear set benefit of being able to write the server side code in Python, it is fantastically readable. Additionally, Python has a great module system out of the box and allows programmers to blend imperative with functional programming styles. PIP exists as a stable package manager and there is a large community for support.

Tornado and GEvent are the standard event-driven libraries, but their support for WebSockets  could be better. Only one of the libraries \url{http://sockjs.org/} remains up to date. However, for this project, it would be favourable to use Socket.io and none of the Tornado extensions for Socket.io are up to date. Still, Python + Tornado make for a strong choice.

\subsubsection{Node (Express)}
Node is a relatively new player in the evented programming world, released in 2009. Since then, it has gained some serious traction over the last couple of years\cite{node-industry}\cite{node-excite}\cite{node-python}. Node's stream support has a very clean API which allows 3rd party libararies to implement the stream interface, which then allows streams to be redirected, paused, intercepted and read with ease. This would make writing code to handle audio streams, far simpler.

One of the other benefits of using Node, that would arise if it is decided that the client will be written in Javascript, is that Node is also Javascript. Working in a single language is not only useful where productivity is concerned, but also allows the client and server to share common modules.

Node is however, still in beta, meaning that it is not fully insured for production. This can lead to slightly unstable APIs, but more often immature libaries. A good way to check how reliable a technology is (at this stage) is to see how many, and which companies are using it in production. The large names from the industry list on Node's website include: PayPal, Yahoo, Microsoft, LinkedIn, eBay and The New York Times. If Node can be reliable enough for companies of that calibre, then it is reasonable to expect that it could be feasible for this project.
